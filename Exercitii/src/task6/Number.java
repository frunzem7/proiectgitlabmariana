package task6;

public class Number {
    private int number;

    public int getNumber() {
        return number;
    }

    public void setNumber(int newValue) {
        number = newValue;
    }
}