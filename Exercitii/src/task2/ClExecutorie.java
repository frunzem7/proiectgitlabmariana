package task2;

public class ClExecutorie extends ClasaDeBaza {
    public static void main(String[] args) {

        ClasaDeBaza clasaDeBaza = new ClasaDeBaza();
        ClTata clTata = new ClTata();
        ClMama clMama = new ClMama();
        ClCopil1 clCopil1 = new ClCopil1();
        ClCopil2 clCopil2 = new ClCopil2();
        ClCopil3 clCopil3 = new ClCopil3();
        ClBunelul clBunelul = new ClBunelul();
        ClBunica clBunica = new ClBunica();

        clasaDeBaza.cineCumparaPaine();
        clTata.cineCumparaPaine();
        clBunelul.cineCumparaPaine();
        clMama.cineCumparaPaine();
        clBunica.cineCumparaPaine();
        clCopil1.cineCumparaPaine();
        clCopil2.cineCumparaPaine();
        clCopil3.cineCumparaPaine();
    }
}
