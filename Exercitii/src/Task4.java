import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("Introduce depozitul: ");
        double deposit = sc.nextInt();

        System.out.print("Introduce rata: ");
        double rata = sc.nextInt();

        System.out.print("Introduce ani: ");
        int years = sc.nextInt();

        double formula1 = 0;
        double formula2 = 0;

        for (int i = 1; i <= years; i++) {
            formula1 = deposit * rata / 100;
            deposit += formula1;
            formula2+=formula1;

            System.out.println("Profit: " + formula1);
            System.out.println("Suma totala: " + deposit);
        }
        System.out.println();
        System.out.println("Suma depozitului peste " + years + " ani se va mari cu " + formula2 + " si va constitui " + deposit + " lei.");
    }
}