package task3;

import java.util.Scanner;

public class Main {
    static void test(String login, String password, String confirmPassword) throws Exception {
        if (!password.equals(confirmPassword)) {
            throw new WrongPasswordException("Password and confrimPassword not equals!");
        }
        if (login.matches(".*[a-z].*") & login.matches(".*[0-9].*") & login.matches(".*[_].*") & login.length() < 20) {
            System.out.println("'" + login + "' este introdus corect");
        } else {
            throw new WrongLoginException("Login este introdusa incorect: " + login);
        }

        if (password.matches(".*[a-z].*") & password.matches(".*[0-9].*") & password.matches(".*[_].*") & password.length() < 20) {
            System.out.println("'" + password + "' este introdus corect");
        } else {
            throw new WrongPasswordException("Parola este introdusa incorect: " + password);
        }
    }

    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);

        System.out.print("Introduceti login: ");
        String login = sc.next();

        System.out.print("Introduceti parola: ");
        String pasword = sc.next();

        System.out.print("Introduceti parola din nou: ");
        String confirmPassword = sc.next();

        test(login, pasword, confirmPassword);
    }
}
