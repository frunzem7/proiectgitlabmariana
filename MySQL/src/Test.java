import java.sql.*;
import java.util.Scanner;

public class Test {
    private static final String url = "jdbc:mysql://localhost:3306/test";
    private static final String user = "root", pass = "root";

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Scrie tara de origine: ");
        String b = sc.next();

        System.out.print("Scrie capitala: ");
        String c = sc.next();

        try {

            Class.forName("com.mysql.cj.jdbc.Driver");

            Connection connect = DriverManager.getConnection(url, user, pass);

            Statement statement = connect.createStatement();

            statement.executeUpdate("insert into corporation (country, city) values('" + b + "','" + c + "')");

            ResultSet resultSet = statement.executeQuery("select * from corporation ");

            while (resultSet.next()) {
                int id = resultSet.getInt(1);
                String country = resultSet.getString(2);
                String city = resultSet.getString(3);
                System.out.printf("%s %s %s%n", id, country, city);
            }

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}