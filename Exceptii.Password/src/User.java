import java.util.Objects;

public class User {
    private final static String REGEX = "^[A-Za-z0-9=.,:\\s-_]+$";
    private final static int MIN_LENGTH = 2;
    private final static int MAX_LENGTH = 19;

    private final String login;
    private final String password;
    private final String confirmPassword;

    public User(String login, String password, String confirmPassword) {
        Objects.requireNonNull(login, "The login must not be null!");
        Objects.requireNonNull(password, "The password must not be null!");
        Objects.requireNonNull(confirmPassword, "The confirmPassword must not be null!");

        Guards.isTrue(password.equals(confirmPassword), "Password and ConfirmPassword must be equals");

        Guards.match(login, "The login must much be pattern.", REGEX);
        Guards.match(password, "The password must much be pattern.", REGEX);
        Guards.match(confirmPassword, "The confirmPassword must much be pattern.", REGEX);

        Guards.minLength(login, String.format("The login must have only %s characters", MIN_LENGTH), MIN_LENGTH);
        Guards.minLength(password, String.format("The password must have only %s characters", MIN_LENGTH), MIN_LENGTH);
        Guards.minLength(confirmPassword, String.format("The confirmPassword must have only %s characters", MIN_LENGTH), MIN_LENGTH);

        Guards.maxLength(login, String.format("The login must have only %s characters", MAX_LENGTH), MAX_LENGTH);
        Guards.maxLength(password, String.format("The password must have only %s characters", MAX_LENGTH), MAX_LENGTH);
        Guards.maxLength(confirmPassword, String.format("The confirmPassword must have only %s characters", MAX_LENGTH), MAX_LENGTH);

        this.login = login;
        this.password = password;
        this.confirmPassword = confirmPassword;
    }

    @Override
    public String toString() {
        return "User{" +
                "login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", confirmPassword='" + confirmPassword + '\'' +
                '}';
    }
}