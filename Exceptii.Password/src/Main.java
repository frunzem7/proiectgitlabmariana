import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Login: ");
        String login = scanner.next();

        System.out.print("Password: ");
        String password = scanner.next();

        System.out.print("ConfirmPassword: ");
        String confirmPassword = scanner.next();

        User user = new User(login, password, confirmPassword);

        System.out.println(user);
    }
}

