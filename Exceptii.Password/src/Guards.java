public class Guards {

    public static void match(String value, String message, String regex) {
        if (!value.matches(regex)) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void minLength(String value, String message, Integer length) {
        if (value.length() <= length) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void maxLength(String value, String message, Integer length) {
        if (value.length() > length) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void isTrue(Boolean value, String message) {
        if (!value) {
            throw new IllegalArgumentException(message);
        }
    }
}

