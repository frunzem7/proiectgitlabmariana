public class Degrees {
    private final int note1;
    private final int note2;
    private final int noteExam;
    private double media;

    public Degrees(int note1, int note2, int noteExam) {
        this.note1 = note1;
        this.note2 = note2;
        this.noteExam = noteExam;
        this.media = (note1 + note2 + noteExam) / 3.0;
    }

    public int getNote1() {
        return note1;
    }

    public int getNote2() {
        return note2;
    }

    public int getNoteExam() {
        return noteExam;
    }

    public double getMedia() {
        return media;
    }

    @Override
    public String toString() {
        return "Specialty{" +
                "note1=" + note1 +
                ", note2=" + note2 +
                ", noteExam=" + noteExam +
                ", media=" + media +
                '}';
    }
}
