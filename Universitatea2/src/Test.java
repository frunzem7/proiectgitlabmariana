import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        test(sc);
    }

    public static void test(Scanner sc) {
        System.out.println("Intrare in program...");

        while (true) {
            System.out.print("Introduceti facultatea (matematica/informatica): ");
            String faculty = sc.next();

            if (faculty.equals("matematica")) {
                System.out.println("A fost aleasa facultatea matematica.");
                addStudent(sc);
                break;
            } else if (faculty.equals("informatica")) {
                System.out.println("A fost aleasa facultatea informatica.");
                addStudent(sc);
                break;
            } else {
                System.out.println("Nu ati introdus facultatea corect!");
            }
        }
        System.out.println("Iesire din program...");
    }

    public static void addStudent(Scanner sc) {
        while (true) {
            System.out.print("Doriti sa introduceti studenti noi? (da/nu): ");
            String choose = sc.next();
            if (choose.equals("da")) {
                System.out.print("Creati numele grupei noi: ");
                String groupName = sc.next();

                System.out.print("Introduceti numele: ");
                String name = sc.next();

                System.out.print("Introduceti anul nasterii: ");
                int year = sc.nextInt();

                System.out.print("Introduceti specialitatea (algebra/geometrie/calcul integral): ");
                String speciality = sc.next();

                System.out.print("Introduceti nota 1: ");
                int note1 = sc.nextInt();

                System.out.print("Introduceti nota 2: ");
                int note2 = sc.nextInt();

                System.out.print("Introduceti nota examen: ");
                int noteExamen = sc.nextInt();

                Student student = new Student(speciality, groupName, name, year, new Degrees(note1, note2, noteExamen));
                if (student.getDegrees().getMedia() > 9) {
                    System.out.println("%s Esti promovat!".formatted(student));
                } else {
                    System.out.println("%s Nu ai trecut cu brio!".formatted(student));
                }
            } else if (choose.equals("nu")) {
                break;
            } else {
                throw new IllegalArgumentException("Unknown answer");
            }
        }
    }

}
