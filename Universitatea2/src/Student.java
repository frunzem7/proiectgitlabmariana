import java.util.Calendar;

public class Student {
    private final String speciality;
    private final String groupName;

    private final String name;
    private final Integer year;
    private final Degrees degrees;
    private Integer age;

    public Student(String speciality, String groupName, String name, Integer year, Degrees degrees) {
        if (!speciality.equals("algebra") && !speciality.equals("geometrie") && !speciality.equals("calculul integral")) {
            throw new IllegalArgumentException("Unknown speciality");
        }
        this.speciality = speciality;
        this.groupName = groupName;
        this.name = name;
        this.year = year;
        this.degrees = degrees;
        this.age = Calendar.getInstance().get(Calendar.YEAR) - year;
    }

    public String getSpeciality() {
        return speciality;
    }

    public String getGroupName() {
        return groupName;
    }

    public String getName() {
        return name;
    }

    public Integer getYear() {
        return year;
    }

    public Degrees getDegrees() {
        return degrees;
    }

    public Integer getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "groupName='" + groupName + '\'' +
                ", name='" + name + '\'' +
                ", year=" + year +
                ", degrees=" + degrees +
                ", age=" + age +
                '}';
    }
}
