package library.book;

import library.author.Author;

public class TestBook {
    public static void main(String[] args) {
        Author anAuthor = new Author("Alan Kay", "alankay88@gmail.com", 'm');
        Book aBook = new Book("Java pentru incepatori", anAuthor, 19.95, 1000);
        Book anotherBook = new Book("Mai multe Java pentru incepatori", new Author("Ruxandra", "ruxy@gmail.com", 'f'), 29.95, 850);

        System.out.println(aBook.getAuthor().getName());
        System.out.println(aBook.getAuthor().getEmail());

        System.out.println();
        System.out.println(anAuthor.getName());
        System.out.println(anAuthor.getEmail());
        System.out.println(anAuthor.getGender());

        System.out.println();
        System.out.println(anotherBook.getAuthor());
        System.out.println(anotherBook.getAuthorEmail());
        System.out.println(anotherBook.getAuthorGender());
    }
}
