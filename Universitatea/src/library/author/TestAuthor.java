package library.author;

public class TestAuthor {
    public static void main(String[] args) {
        Author anAuthor = new Author("Alan Kay", "alankay@books.com", 'm');
        System.out.println(anAuthor);//call toString()

        anAuthor.setEmail("alankay88@gmail.com");
        System.out.println(anAuthor);//call toString()

        System.out.println();
        System.out.println(anAuthor.getName());
        System.out.println(anAuthor.getEmail());
        System.out.println(anAuthor.getGender());
    }
}
